# This script will generate 32 config files for the TREX
#
#
#
#
import argparse
import sys

Nboards = 16

def xmlTemplate(Alias, PrimaryName):
   TrexBoardPath = "/opc/TrexBoard.xml"
   ConfigXsd     = "Configuration.xsd"
   
   xmlTemplateString = f"""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE configuration [
<!ENTITY TREX SYSTEM \"{TrexBoardPath}\">
]>

<configuration xmlns=\"http://cern.ch/quasar/Configuration\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://cern.ch/quasar/Configuration {ConfigXsd}\">
      <Trex name=\"{Alias}\" hostname=\"{PrimaryName}\">
        &TREX;
      </Trex>

</configuration>
"""
   return xmlTemplateString

# For the A-side
for nb in range(Nboards):
   Alias         = f"epc-l1c-trxA-{nb:02}"
   PrimaryName   = f"epc-l1c-trex-{nb + 6:02}"
   with open(f"configFiles/Config_{Alias}.xml","w") as cfile:
      cfile.write(xmlTemplate(Alias, PrimaryName))

# For the C-side
for nb in range(Nboards):
   Alias         = f"epc-l1c-trxC-{nb:02}"
   PrimaryName   = f"epc-l1c-trex-{nb + 22:02}"
   with open(f"configFiles/Config_{Alias}.xml","w") as cfile:
      cfile.write(xmlTemplate(Alias, PrimaryName))
