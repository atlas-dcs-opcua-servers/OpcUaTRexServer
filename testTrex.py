# This is just for testing Poverty.
# Copy or symlink it into build/bin of your quasar OPC-UA server project.

import sys
import os
import time
import readline, rlcompleter

if not os.path.isfile('Poverty.so'):
    raise Exception("Probably running from a wrong directory. Put this script in the dir where Poverty.so is")
sys.path.append('') # add current dir, as it is where Poverty probably is.

import Poverty

# the thing below is for interactive python usage.
readline.parse_and_bind("tab: complete")

backend_config = os.path.sep.join(['..', 'bin', 'ServerConfig.xml'])
config = os.path.sep.join(['..', 'bin', 'config.xml'])



Poverty.Poverty.startServer(backend_config, config)
time.sleep(2)


predator      = Poverty.Poverty.getFPGA('Trex.trex_predator_sysmon_master')
predatorSlave = Poverty.Poverty.getFPGA('Trex.trex_predator_sysmon_slave')
dino1         = Poverty.Poverty.getFPGA('Trex.trex_dino_1_xadc')
dino2         = Poverty.Poverty.getFPGA('Trex.trex_dino_2_xadc')
dino3         = Poverty.Poverty.getFPGA('Trex.trex_dino_3_xadc')
dino4         = Poverty.Poverty.getFPGA('Trex.trex_dino_4_xadc')


# some other usage example: (OpcUaEFex)
# v1=Poverty.Poverty.getVoltage('sh1.b1.m1.fpga1.v1')
# v1.setVoltage(4, Poverty.StatusCode.Good)
