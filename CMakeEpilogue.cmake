# Configure installation
#
# default CMAKE_INSTALL_PREFIX is /usr/local
# To install:
#   cmake --install build
#
set ( CMAKE_INSTALL_PREFIX "/opc" )
#
message("CMAKE_INSTALL_PREFIX is ${CMAKE_INSTALL_PREFIX}")
message("Executable is ${EXECUTABLE}")
message("$PROJECT_SOURCE_DIR is ${PROJECT_SOURCE_DIR}")
message("$PROJECT_BINARY_DIR is ${PROJECT_BINARY_DIR}")
message("$SERVER_SHARED_LIB_TARGET is ${SERVER_SHARED_LIB_TARGET}") # .so and include lib directory

# For installation set the rpath for Poverty.so so can find SERVER_SHARED_LIB_TARGET
set_target_properties( PovertyPython
  PROPERTIES INSTALL_RPATH
  ${CMAKE_INSTALL_PREFIX}
  )

install(TARGETS ${EXECUTABLE} DESTINATION ${CMAKE_INSTALL_PREFIX} )
install(TARGETS ${SERVER_SHARED_LIB_TARGET} PovertyPython LIBRARY DESTINATION ${CMAKE_INSTALL_PREFIX} )
install(FILES
  ${PROJECT_SOURCE_DIR}/bin/config.xml
  ${PROJECT_SOURCE_DIR}/bin/ServerConfig.xml
  ${PROJECT_BINARY_DIR}/Configuration/Configuration.xsd
  DESTINATION
  ${CMAKE_INSTALL_PREFIX}
  )
message("DONE INSTALL")

