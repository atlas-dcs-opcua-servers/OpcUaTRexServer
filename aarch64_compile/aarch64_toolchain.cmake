set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

set(CMAKE_SYSROOT /scratch/x01/zynq/cc7-root)
SET(CMAKE_C_COMPILER
/scratch/x01/tmkrtchy/compile/cross/bin/aarch64-linux-gcc)

SET(CMAKE_CXX_COMPILER
/scratch/x01/tmkrtchy/compile/cross/bin/aarch64-linux-g++)

SET(CMAKE_FIND_ROOT_PATH /scratch/x01/zynq/cc7-root)
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_CXX_FLAGS "-D_GLIBCXX_USE_CXX11_ABI=0")
