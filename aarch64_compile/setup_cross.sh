
cp CMakeLists_aarch64.txt ../CMakeLists.txt
cp CMakeLists_Poverty.txt ../Poverty/CMakeLists.txt

export PREFIX=/scratch/x01/tmkrtchy/compile
export INSTALL_DIR=${PREFIX}/cross

export PATH=${INSTALL_DIR}/bin:$PATH
export LD_LIBRARY_PATH=${INSTALL_DIR}/lib64:${INSTALL_DIR}/aarch64-linux/lib64/:$LD_LIBRARY_PATH
